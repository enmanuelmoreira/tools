#!/bin/bash
find . \
    -maxdepth 2 -type d \
    -name ".git" \
    -execdir python3 -c 'import os; print(os.path.abspath("."))' \; \
    -execdir git pull --all \;
