#!/bin/bash
echo
echo
echo 'Existing lock files'
ls -l /run/lock/qemu-server
while read -p 'Enter the VM ID here to delete corresponding lock, press Enter key to exit e.g. 101: ' vmid; do
echo
echo
echo 'Existing lock files'
ls -l /run/lock/qemu-server
if [[ "$vmid" = "" ]] || [[ "$vmid" = "q" ]] ;
then       
exit
elif [[ "$vmid" -gt 0 ]] && [[ "$vmid" -lt 1000000000 ]];
then
qm unlock $vmid
rm -f /run/lock/qemu-server/lock-$vmid.conf
qm unlock $vmid
ls -l /run/lock/qemu-server
else
echo 'Input error, please enter correct VM ID.'
fi
done